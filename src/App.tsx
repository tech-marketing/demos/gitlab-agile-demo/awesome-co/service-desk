import React, { useState } from 'react';
import FeatureForm from './FeatureForm'
import './App.css';


function App() {
  const [success, setSuccess] = useState()
  
  if (success) {
   return (
    <div>
      Your request has been submitting. 
      <div>
        <button onClick={() => setSuccess(undefined) }>
          Submit another request
        </button>
      </div>
      
    </div>
   )
  }

  return (
    <div>
      <FeatureForm setSuccess={setSuccess} />
    </div>
  );
}

export default App;

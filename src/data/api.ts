import axios from 'axios'
import { FeatureFormValues, Project } from '../interfaces'



export const PROJECTID = {
  webApp: '26833848',
  mobileApp: '26833843'
}

const TOKEN = 'glpat-hHMHceL373RuW9zWyLDo'

const baseURL = (id: keyof Project) => {
  const project = PROJECTID[id]
  return `https://gitlab.com/api/v4/projects/${project}/issues?access_token=${TOKEN}`
}

const issueDescription = (values: FeatureFormValues) => `
## Summary

${values.description}

## Requestor

Name: ${values.firstName} ${values.lastName}\
Email: ${values.email}

/label ~"wf::1-triage"
`

const createIssue = (values: any, setSuccess: any) => {
  
  
  const description = issueDescription(values)
  
  axios.post(baseURL(values.application), {
    title: values.requestTitle,
    description
  }, {
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(() => setSuccess('TRUE')).catch(e => console.log(e))
}

export default createIssue
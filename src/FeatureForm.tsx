import React from 'react';
import { Formik } from "formik";
import validate from './validate'
import createIssue from './data/api'

const Form = (props: any) => {
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
  } = props;

  return (
    <form onSubmit={handleSubmit}>
        <label htmlFor="firstName">First Name</label>
        <input
          id="firstName"
          name="firstName"
          type="text"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.firstName}
        />
        {touched.firstName && errors.firstName ? <div>{errors.firstName}</div> : null}
  
        <label htmlFor="lastName">Last Name</label>
        <input
          id="lastName"
          name="lastName"
          type="text"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.lastName}
        />
        {touched.lastName && errors.lastName ? <div>{errors.lastName}</div> : null}
   
        <label htmlFor="email">Email Address</label>
        <input
          id="email"
          name="email"
          type="email"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.email}
        />
        {touched.email && errors.email ? <div>{errors.email}</div> : null}

        <label htmlFor="application">Application</label>
        <select
          name="application"
          id="application"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.application}
        >
          <option value="" label="Please choose an option">Please choose an option</option>
          <option value="webApp" label="Web App">Web App</option>
          <option value="mobileApp" label="Mobile App">Mobile App</option>
        </select>


        <label htmlFor="requestTitle">Request Title</label>
        <input
          id="requestTitle"
          name="requestTitle"
          type="text"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.requestTitle}
        />
        
        <label htmlFor="description">Description</label>
        <textarea
          id="description"
          name="description"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.description}
        />

        <div>
          <button type="submit">Submit</button>
        </div>
      </form>
  )
}

const FeatureForm = ({ setSuccess }: any) => (
  <div>
    <h1>Submit a request</h1>
    <Formik
      initialValues={{
         firstName: '',
         lastName: '', 
         email: '',
         application: '',
         requestTitle: '',
         description: '' 
      }}
      onSubmit={(values: any) => createIssue(values, setSuccess)}
      validate={validate}
    >
      {props => (
        <Form {...props} />
      )}
    </Formik>
  </div>
)

export default FeatureForm
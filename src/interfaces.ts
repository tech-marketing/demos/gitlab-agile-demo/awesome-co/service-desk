export type Project = { webApp: string, mobileApp: string}

export interface FeatureFormValues {
    firstName: string;
    lastName: string;
    email: string;
    application: string;
    requestTitle: string;
    description: string
}